﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S17_DavidController : MonoBehaviour
{
    public ParticleSystem confetti;
    public AudioSource audio;
    public S17_RotateStatue statue;

    private Quaternion initialRotation;

    private void Start()
    {
        Invoke("End", 3f * 60f);

        initialRotation = statue.transform.rotation;
    }

    private void End()
    {
        SceneManager.LoadScene(0);
    }

    public void Done()
    {
        StartCoroutine(DoneCenas());
    }

    private IEnumerator DoneCenas()
    {
        statue.enabled = false;
        
        while (Quaternion.Angle(statue.transform.rotation, initialRotation) > 1f)
        {
            statue.transform.rotation = Quaternion.Lerp(statue.transform.rotation, initialRotation, Time.deltaTime * 10f);

            yield return null;
        }

        confetti.Play();
        audio.Play();

        yield return new WaitForSeconds(10f);

        End();
    }
}
