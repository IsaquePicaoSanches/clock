﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S17_RotateStatue : MonoBehaviour
{
    void Start() {
        Cursor.visible = true;    
    }
    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            transform.eulerAngles += new Vector3(0, Input.GetAxis("Mouse X") * -50, 0) * Time.deltaTime * 10f;
        }
    }
}
