﻿using System;
using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Collider))]
public class PaintMe : MonoBehaviour {

    public Camera mainCamera;
    public S17_DavidController davidController;

    public Color ClearColour;
    public Material PaintShader;
    public RenderTexture PaintTarget;
    private RenderTexture TempRenderTarget;
    private Material ThisMaterial;

    private Texture2D tex;
    private bool _isDone;

    void Init()
    {
        if (ThisMaterial == null)
            ThisMaterial = this.GetComponent<Renderer>().material;

        //	already setup
        if (PaintTarget != null)
            if (ThisMaterial.mainTexture == PaintTarget)
                return;

        //	copy texture
        if (ThisMaterial.mainTexture != null)
        {
            if (PaintTarget == null)
                PaintTarget = new RenderTexture(ThisMaterial.mainTexture.width, ThisMaterial.mainTexture.height, 0);
            Graphics.Blit(ThisMaterial.mainTexture, PaintTarget);
            ThisMaterial.mainTexture = PaintTarget;
        }
        else
        {
            if (PaintTarget == null)
                PaintTarget = new RenderTexture(1024, 1024, 0);

            //	clear if no existing texture
            Texture2D ClearTexture = new Texture2D(1, 1);
            ClearTexture.SetPixel(0, 0, ClearColour);
            Graphics.Blit(ClearTexture, PaintTarget);
            ThisMaterial.mainTexture = PaintTarget;

        }
    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit hitInfo = new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // OnMouseDown
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hitInfo))
            {
                hitInfo.collider.SendMessage("HandleClick", hitInfo, SendMessageOptions.DontRequireReceiver);
            }
        }

    }

    void HandleClick(RaycastHit Hit)
    {
        Vector2 LocalHit2 = Hit.textureCoord;
        PaintAt(LocalHit2);
    }


    void PaintAt(Vector2 Uv)
    {
        Init();

        if (TempRenderTarget == null)
        {
            TempRenderTarget = new RenderTexture(PaintTarget.width, PaintTarget.height, 0);
        }


        PaintShader.SetVector("PaintUv", Uv);
        Graphics.Blit(PaintTarget, TempRenderTarget);
        Graphics.Blit(TempRenderTarget, PaintTarget, PaintShader);

        toTexture2D(TempRenderTarget);

        average = AverageColorFromTexture(tex);

        if (Math.Abs(average.r) < 0.01f && Math.Abs(average.g) < 0.01f && !_isDone)
        {
            _isDone = true;
            davidController.Done();
        }
    }

    private Color average;

    private void toTexture2D(RenderTexture rTex)
    {
        if (tex == null)
        {
            tex = new Texture2D(512, 512, TextureFormat.RGB24, false);
        }

        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
    }

    private Color32 AverageColorFromTexture(Texture2D tex)
    {
        Color32[] texColors = tex.GetPixels32();

        int total = texColors.Length;

        float r = 0;
        float g = 0;
        float b = 0;

        for (int i = 0; i < total; i++)
        {
            r += texColors[i].r;
            g += texColors[i].g;
            b += texColors[i].b;
        }

        return new Color32((byte)(r / total), (byte)(g / total), (byte)(b / total), 0);
    }
}
