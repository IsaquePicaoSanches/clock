﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s9_main : MonoBehaviour {
    public Transform bag;

    public GameObject intro1, intro2, help1, help2, help3;
    bool frozen;

    private void Start() {
        //Cursor.visible = true;
        Invoke("End", 3 * 60f);
        StartCoroutine(Playthrough());
    }

    void End() {
        SceneManager.LoadScene(0);
    }

    IEnumerator Playthrough() {
        frozen = true;
        intro1.SetActive(true);
        yield return new WaitForSeconds(2);
        intro2.SetActive(true);
        yield return new WaitForSeconds(3);
        frozen = false;
        intro1.SetActive(false);
        intro2.SetActive(false);
        yield return new WaitForSeconds(10);
        help1.SetActive(true);
        yield return new WaitForSeconds(2);
        help1.SetActive(false);
        yield return new WaitForSeconds(10);
        help1.SetActive(true);
        yield return new WaitForSeconds(2);
        help1.SetActive(false);
        yield return new WaitForSeconds(10);
        help2.SetActive(true);
        yield return new WaitForSeconds(2);
        help2.SetActive(false);
        yield return new WaitForSeconds(10);
        End();
    }

    int count = 0;
    void Update() {
        float y = 0;
        if (Input.anyKey && !frozen)
            y = 2.8f;
        else
            y = 4.9f;
        bag.transform.position = Vector3.Lerp(bag.transform.position, new Vector3(0, y, 0), Time.deltaTime * 10f);

        if (Input.anyKeyDown)
            count++;

    }
}
