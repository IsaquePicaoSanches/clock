﻿using UnityEngine;

public class S8_DecorItem : MonoBehaviour
{
    public static int order = 100;

    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        order++;
        _spriteRenderer.sortingOrder = order;
    }

    private void OnMouseDrag()
    {
        var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.position = new Vector3(pos.x, pos.y, 0);
    }
}
