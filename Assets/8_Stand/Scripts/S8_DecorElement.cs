﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class S8_DecorElement : MonoBehaviour, IPointerClickHandler
{
    public GameObject decorItem;
    public Sprite sprite;
    public float scale;

    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    private void Start()
    {
        _image.sprite = sprite;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var decor = Instantiate(decorItem, Vector3.zero, Quaternion.identity);

        decor.GetComponent<SpriteRenderer>().sprite = sprite;
        decor.GetComponent<SpriteRenderer>().sortingOrder = S8_DecorItem.order + 1;
        decor.transform.localScale = new Vector3(scale, scale, scale);
        decor.AddComponent<BoxCollider2D>();
    }
}
