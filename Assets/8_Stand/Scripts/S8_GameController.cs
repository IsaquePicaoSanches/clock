﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class S8_GameController : MonoBehaviour
{
    private float _timeLimit = 180f;
    private float _time;

    private void Start()
    {
        _time = _timeLimit;
        Cursor.visible = true;
    }

    public void DoneBtnClick()
    {
        //ScreenCapture.CaptureScreenshot(System.DateTime.Now.Day + "-" + System.DateTime.Now.Hour + "-" + System.DateTime.Now.Minute + ".png");
        SceneManager.LoadScene(0);
    }

    private void Update()
    {
        _time -= Time.deltaTime;

        if (_time <= 0)
        {
            SceneManager.LoadScene(0);
        }
    }
}
