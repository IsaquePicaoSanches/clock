﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

/// <summary>
/// Deal with all the tosting of the bread
/// </summary>
public class S20_GameManager : MonoBehaviour
{
   [Header("Images: ")]
   [SerializeField,Tooltip("Torrada Feita")] private GameObject _feito = null;
   [SerializeField] private SpriteRenderer _feitaImage;
   [SerializeField,Tooltip("Sentences sprites")] private Sprite[] _sentences = null;
   private int _senteceNumber = 0;
   [SerializeField] private GameObject _click = null;
   
   [Header("Animations: ")]
   [SerializeField] private Animator _leverAnimator = null;

   [Header("Audio: ")] 
   [SerializeField] private AudioSource _audioSource = null;
   [SerializeField] private AudioClip _toastDown, _toastDone, _angels, _cooking;
   
   [Header("Timmings: ")]
   [SerializeField,Tooltip("Total time in seconds")] private float _totalTime = 180;
   private float _timeLeft = 0;
   private bool _atTheEnd = false;
   [SerializeField] private float _toastTime = 5f;
   private bool _toasting = false;
   
   private void Start()
   {
      Cursor.visible = true;
   }

   private void Update()
   {
      _timeLeft = _totalTime - Time.deltaTime;
      
      if (Input.anyKeyDown && _atTheEnd || _timeLeft <= 0){
         SceneManager.LoadScene(0);
      }
   }

   /// <summary>
   /// Start doing things when clicking on the lever
   /// </summary>
   public void ClickLever()
   {
      if (!_toasting){
         _toasting = true;
         _click.SetActive(false);
         StartCoroutine(TOAST());
      }
   }

   /// <summary>
   /// Make a toast
   /// </summary>
   /// <returns></returns>
   private IEnumerator TOAST()
   {
      _leverAnimator.SetTrigger("Toast");
      _audioSource.PlayOneShot(_toastDown);
      _audioSource.PlayOneShot(_cooking);
      yield return  new WaitForSeconds(_toastTime);
      _senteceNumber = Random.Range(0, _sentences.Length);
      _feitaImage.sprite = _sentences[_senteceNumber];
      _leverAnimator.SetTrigger("Untoast");
      _audioSource.PlayOneShot(_toastDone);
      _audioSource.PlayOneShot(_angels);
      yield return new WaitForSeconds(3f);
      _toasting = false;
      _atTheEnd = true;
   }
}
