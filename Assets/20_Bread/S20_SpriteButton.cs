﻿using UnityEngine;
using UnityEngine.Events;

public class S20_SpriteButton : MonoBehaviour
{
    [SerializeField] private UnityEvent _clickEvent = null;
    
    private void OnMouseDown() 
    {
        _clickEvent.Invoke();
    }
}
