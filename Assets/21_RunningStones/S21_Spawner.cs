﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class S21_Spawner : MonoBehaviour
{
    public List<GameObject> ImagesToSpawn;
    public List<string> Texts;
    public GameObject EndBoss;

    private List<GameObject> imagesSpawned;
    private float spawnIn = 3f, spawnInRandom = 1f, nextSpawn, speed = 5f;
    private int counter = 0, totalInGame = 10;



    // Start is called before the first frame update
    void Start()
    {
        nextSpawn = Time.time + spawnIn + Random.Range(-spawnInRandom, spawnInRandom);
        imagesSpawned = new List<GameObject>();
        Invoke("EndGame", 3*60f);
    }

    // Update is called once per frame
    void Update()
    {
        if (nextSpawn <= Time.time && counter < totalInGame)
        {
            nextSpawn = Time.time + spawnIn + Random.Range(-spawnInRandom, spawnInRandom);
            var n = Instantiate(ImagesToSpawn[Random.Range(0,ImagesToSpawn.Count)], transform.position, transform.rotation);
            imagesSpawned.Add(n);
            var t = n.GetComponentInChildren<Text>();
            t.text = Texts[Random.Range(0, Texts.Count)];
            t.enabled = false;
            counter++;
            if (counter == totalInGame)
                StartCoroutine(EndGameCoroutine(5f));
        }

        List< GameObject > toDelete = new List<GameObject>();

        foreach (var image in imagesSpawned)
        {
            if (image.transform.position.x < -15)
                toDelete.Add(image);
            else
                image.transform.Translate(Vector2.left * speed * Time.deltaTime);

            if (image.transform.position.x < -5)
                image.GetComponentInChildren<Text>().enabled = true;
        }

        imagesSpawned = imagesSpawned.Except(toDelete).ToList();
        foreach (var d in toDelete)
        {
            Destroy(d);
        }
    }

    private IEnumerator EndGameCoroutine(float inSeconds)
    {
        bool endGame = false;
        yield return new WaitForSeconds(inSeconds);
        while (!endGame)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            var p = EndBoss.transform.position;
            p.x = Mathf.Lerp(p.x, 0, Time.deltaTime * speed);
            EndBoss.transform.position = p;

            if (p.x <= 1)
            {
                endGame = true;
                //Invoke("EndGame", 4f);
                Invoke("ShootRunner", 1f);
            }

        }
    }

    public GameObject map;
    private void ShootRunner()
    {
        Invoke("KillRunner", .5f);
        EndBoss.transform.GetChild(0).gameObject.SetActive(true);
        Invoke("Bug", 2f);
    }

    void Bug() {
        map.gameObject.SetActive(true);
        Invoke("EndGame", 17f);
    }

    private void KillRunner()
    {
        Destroy(GameObject.Find("Runner"));
    }

    private void EndGame() //End Scene
    {
        SceneManager.LoadScene(0);
    }
}
