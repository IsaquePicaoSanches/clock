﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S21_Runner : MonoBehaviour
{
    private bool jumping = false;
    private float force = 35f;
    private Rigidbody2D rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !jumping)
        {
            jumping = true;
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
            rb.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            
            Invoke("JumpEnable", .7f);

        }
    }

    private void JumpEnable()
    {
        jumping = false;
    }
    
}
