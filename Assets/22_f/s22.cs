﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class s22 : MonoBehaviour {

    public Text message, answerA, answerB;
    public s22_selection optionA, optionB;
    int count = 0;
    public Animator animator;
    float timer = 0;

    void Start() {
        Cursor.visible = true;

        question = "WE KNOW WHO YOU ARE, DIMITRI.";
        aA = "WHERE AM I?";
        aB = "WHO ARE YOU?";

        StartCoroutine(End());
    }

    int phase = 0;
    public GameObject music, panelA, panelB, end;
    void Update() {
        if (count == 18) {
            StopAllCoroutines();
            if (Input.GetMouseButtonUp(0))
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            return;
        }
        if (Input.GetMouseButtonUp(0)) {
            phase++;
            if (phase == 2) {
                StartCoroutine(Animate(false));
                return;
            }
        }

        music.SetActive(phase >= 2);
        panelA.SetActive(phase == 0);
        panelB.SetActive(phase == 1);

        timer = Mathf.Max(timer - Time.deltaTime, 0);
        animator.SetBool("talking", timer > 0);
        answerA.color = new Color(1f, 1f, 1f, optionA.selected ? 1f : 0.5f);
        answerB.color = new Color(1f, 1f, 1f, optionB.selected ? 1f : 0.5f);
        //answerA.GetComponent<Button>().maicolor = Color.Lerp(answerA.color, new Color(1f, 1f, 1f, 0f), 2f * Time.deltaTime);
        //answerB.color = Color.Lerp(answerB.color, new Color(1f, 1f, 1f, 0f), 2f * Time.deltaTime);
    }

    string question, aA, aB;

    IEnumerator Animate(bool instant) {
        char[] c = question.ToCharArray();
        message.text = "";
        answerA.text = "";
        answerB.text = "";
        answerA.gameObject.SetActive(false);
        var colors = answerA.GetComponent<Button>().colors;
        colors.normalColor = new Color(1f, 1f, 1f, 0f);
        answerB.GetComponent<Button>().colors = answerA.GetComponent<Button>().colors = colors;
        answerB.gameObject.SetActive(false);

        float t = 0;
        if (c.Length < 30)
            t = 0.07f;
        else
            t = 0.04f;

        for (int i = 0; i < c.Length; i++) {
            message.text += c[i];

            if (instant)
                yield return null;
            else
                yield return new WaitForSeconds(((
                (c[i] == '.' || c[i] == '?' || c[i] == '!')
                && c[(i + 1) % c.Length] == ' ') ? 0.5f : t));
        }
        answerA.gameObject.SetActive(true);
        answerB.gameObject.SetActive(true);
        answerA.text = aA;
        answerB.text = aB;
    }

    public void Next(bool t) {
        timer = 3;
        bool instant = false;
        if (count == 0) {
            question = "DIMITRI, THE LEGEND. THE SUPER AGENT. YOUR REPUTATION PRECEDES YOU.";
            aA = "LET ME OUT!";
            aB = "DID YOU PUT THESE HANDCUFFS ON ME?";
        }
        if (count == 1) {
            question = "YOUR INTEL ON MOSSAD WAS RIGHT. BUT YOU'RE BECOMING RECKLESS.";
            aA = "YOU'RE GOING TO REGRET THIS...";
            aB = "WHO DO YOU WORK FOR?";
        }
        if (count == 2) {
            question = "LIKE THE STUFF YOU PULLED ON THE CHINESE GOVERNMENT... PEOPLE HIGHER UP ARE WORRIED ABOUT YOU.";
            aA = "[LIE] YOU HAVE THE WRONG GUY.";//YOU're going to regret this // You're making a mistake
            aB = "OK. YOU GOT ME. WHAT DO YOU WANT?";
        }
        if (count == 3) {
            question = "WHERE IS THE PACKAGE?";
            aA = "[LIE] WHAT PACKAGE?";
            aB = "I DON'T KNOW THE LOCATION OF THE ASSET.";
        }
        if (count == 4) {
            question = "DON'T LIE TO US...";
            aA = "OR WHAT?";
            aB = "[SILENCE]";
        }
        if (count == 5) {
            question = "WE WANT THE PACKAGE, DIMITRI.";
            aA = "I WANT TO TALK WITH YOUR SUPERIOR.";
            aB = "I CANNOT GIVE YOU THE ASSET.";
        }
        if (count == 6) {
            question = "WE'RE GONNA KILL YOUR FAMILY, DIMITRI.";
            aA = "YOU'RE MAKING A MISTAKE.";
            aB = "[SILENCE]";
        }
        if (count == 7) {
            question = "WHERE IS THE PACKAGE, DIMITRI?";
            aA = "I DON'T HAVE THE PACKAGE!";
            aB = "I CAN'T GIVE YOU THE PACKAGE!";
        }
        if (count == 8) {
            question = "I WANT THE FUCKING PACKAGE, DIMITRI! WHERE IS THE PACKAGE, DIMITRI?";
            aA = "ARE YOU FUCKING DEAF?!";
            aB = "FUCK YOU!";
        }
        if (count == 9) {
            question = "WHERE IS THE F-U-C-K-I-N-G PACKAGE, DIMITRI?";
            aA = "I ALREADY FUCKING TOLD YOU.";
            aB = "I DON'T FUCKING HAVE IT.";
        }
        if (count == 10) {
            question = "I'M GONNA FUCKING KILL YOUR WHOLE FUCKING FAMILY, DIMITRI! FUCK!";
            aA = "GO FUCK YOURSELF, MOTHERFUCKER!";
            aB = "GET FUCKED, YOU FUCKING ASSHOLE!";
        }
        if (count == 11) {
            question = "I FUCKING WANT THE FUCKING PACKAGE MAN OR I'LL FUCKING DO IT RIGHT NOW, RIGHT FUCKING NOW!";
            aA = "I'M GONNA FUCKING DESTROY YOU DO YOU FUCKING HEAR ME?!";
            aB = "I FUCKING SWEAR TO GOD YOU ARE SO FUCKING DEAD!";
        }
        if (count == 12) {
            question = "FUUUUUCK! FUUUCK!";
            aA = "AAAAAAAAARGH!";
            aB = "WAAAAAAAAAAH!";
        }
        if (count == 13) {
            question = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGH!";
            aA = "FUCK YOU!";
            aB = "GO FUCK YOURSELF!";
        }
        if (count == 14) {
            question = "WHERE. IS. T-H-E. F-U-C-K-I-N-G...";
            aA = "MOTHERFUCKER!";
            aB = "FUCKING FUCK!";
        }
        if (count == 15) {
            question = "PA-...";
            aA = "FUCKING FUCK FUCKING SHIT CUNT!";
            aB = "FUCK FUCKING FUCK COCK PISS!";
        }
        if (count == 16) {
            question = "...-CKA-...";
            aA = "FUCK ON THE FUCK SHIT FUCK FUCKING FUCK";
            aB = "FUCK FUCK IN YOUR FUCKING FUCK FUCK FUCK";
        }
        
        end.SetActive(count == 17);            

        //, IF YOU DON'T TELL ME -- RIGHT NOW -- WHERE THE FUCKING PACKAGE IS!
        StopAllCoroutines();
        StartCoroutine(Animate(instant));
        count++;
    }

    IEnumerator End() {
        yield return new WaitForSeconds(60 * 1.5f);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
