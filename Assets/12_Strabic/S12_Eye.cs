﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S12_Eye : MonoBehaviour
{
    public enum RotationDirection
    {
        Positive,
        Negative
    }

    public int key;
    public RotationDirection direction;
    public bool targetAcquired = false;
    public float correctAngle = 35.0f;
    float rotationSpeed = .2f;
    AudioSource audioSource;
    public AudioClip pop;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(RestituteRotation());
    }

    // Update is called once per frame
    void Update()
    {
        if (targetAcquired)
        {
            return;
        }
        if (Input.GetMouseButtonDown(key))
        {
            Vector3 newRot = transform.rotation.eulerAngles;
            if (direction == RotationDirection.Positive)
            {
                newRot.z += 1;
                audioSource.pitch = 1 / (correctAngle / transform.rotation.eulerAngles.z);
                audioSource.Play();
            } else
            {
                newRot.z -= 1;
                float angleForSound = transform.rotation.eulerAngles.z == 0 ? 1 : (360 - transform.rotation.eulerAngles.z);
                audioSource.pitch = 1 / (correctAngle / angleForSound);
                audioSource.Play();
            }

            transform.rotation = Quaternion.Euler(newRot);
        }

        if (direction == RotationDirection.Positive)
        {
            targetAcquired = transform.rotation.eulerAngles.z >= correctAngle;
        }
        else
        {
            Debug.Log("Angle: " + transform.rotation.eulerAngles.z);
            targetAcquired = transform.rotation.z != 0 && transform.rotation.eulerAngles.z <= 360-correctAngle;
        }

        if (targetAcquired)
        {
            audioSource.clip = pop;
            audioSource.Play();
            FindObjectOfType<S12_Person>().TargetAcquired();
        }
    }

    IEnumerator RestituteRotation()
    {
        Quaternion finalRotQuaternion = Quaternion.Euler(Vector3.zero);
        while (!targetAcquired)
        {
            if (transform.rotation != Quaternion.identity)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, finalRotQuaternion, rotationSpeed * Time.deltaTime * 0.5f);
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
