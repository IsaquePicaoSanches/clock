﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S12_Person : MonoBehaviour
{
    int acquiredCount = 0;


    // Start is called before the first frame update
    void Start()
    {
        Invoke("GoToMain", 180.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TargetAcquired()
    {
        acquiredCount++;
        if (acquiredCount == 2)
        {
            Invoke("GoToMain", 3.0f);
        } 
    }

    void GoToMain()
    {
        SceneManager.LoadScene(0);
    }
}
