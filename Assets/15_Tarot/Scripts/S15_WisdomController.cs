﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class S15_WisdomController : MonoBehaviour
{
    public Text wisdomText;

    private void Start()
    {
        Invoke("Exit", 3f * 60f);
    }

    public void Animate(S15_Card card)
    {
        wisdomText.text = card.cardWisdom;
        GetComponent<Animator>().SetTrigger("wisdom");
    }

    public void RotateCard()
    {
        S15_CardController.chosenCard.AnimateBack();
    }

    public void FadeToBlack()
    {
        GetComponent<Animator>().SetTrigger("fade");
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
    }
}
