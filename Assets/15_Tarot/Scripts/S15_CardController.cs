﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class S15_CardController : MonoBehaviour
{
    public Animator cameraAnimator;
    public static S15_Card chosenCard;

    public S15_WisdomController wisdomController;
    public S15_WisdomController fade;

    public List<S15_Card> cardList;

    public static int numberOfClicks;

    List<string> GetStrings() {
        return Resources.Load<EventList>("list").moments.Select(x => x.sentence).ToList();
    }

    string GetMessage() {
        List<string> s = GetStrings();
        return s[Random.Range(0, s.Count)];
    }

    private void Start()
    {
        Cursor.visible = true;
        Invoke("Exit", 3f * 60f);
    }

    public void ShowWisdom(S15_Card card)
    {
        chosenCard = card;
        chosenCard.cardWisdom = GetMessage();
        wisdomController.Animate(card);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && numberOfClicks == 0)
        {
            StartCoroutine(ShuffleCards());
            numberOfClicks = 1;
        }
    }

    public IEnumerator ShuffleCards()
    {
        cardList.Shuffle();

        for (int i = 0; i < cardList.Count; i++)
        {
            cardList[i].MoveToTargetPosition(i == 0);
        }

        cameraAnimator.SetTrigger("zoom");

        yield return new WaitForSeconds(2f);
        
        cardList[0].AnimateBack();
    }

    private void Exit(){
        SceneManager.LoadScene(Random.Range(1, 23));
    }
}
