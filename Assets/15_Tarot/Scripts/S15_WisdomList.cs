﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "S15/Wisdom List")]
public class S15_WisdomList : ScriptableObject
{
    [Multiline] public List<string> wisdomList;

    public void ShuffleWisdoms()
    {
        wisdomList.Shuffle();
    }
}
