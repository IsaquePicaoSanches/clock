﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class S15_Card : MonoBehaviour
{
    public Sprite cardFront;
    public Sprite cardBack;
    
    [Multiline]
    public string cardWisdom;

    private Image _image;
    private Animator _animator;
    private bool _isFront;
    private S15_CardController _cardController;
    private RectTransform _transform;

    private void Awake()
    {
        _image = GetComponent<Image>();
        _animator = GetComponent<Animator>();
        _cardController = GetComponentInParent<S15_CardController>();
        _transform = GetComponent<RectTransform>();
    }


    public void AnimateBack()
    {
        _animator.SetTrigger("rotate");
    }

    public void RotateCard()
    {
        _isFront = !_isFront;
        _image.sprite = _isFront ? cardFront : cardBack;
    }

    public void ShowWisdom()
    {
        if (_isFront)
        {
            _cardController.ShowWisdom(this);
        }
        else
        {
            _cardController.fade.FadeToBlack();
        }
    }

    public void MoveToTargetPosition(bool isTopCard)
    {
        transform.SetSiblingIndex((_cardController.cardList.Count - 1) - _cardController.cardList.IndexOf(this));

        StartCoroutine(MoveToCenter(isTopCard));
    }

    private IEnumerator MoveToCenter(bool isTopCard)
    {
        Vector2 position = new Vector2(0, 0);

        while (Vector2.Distance(transform.position, position) > 0.01f)
        {
            _transform.localPosition = Vector2.Lerp(transform.localPosition, position, Time.deltaTime * 5f);

            yield return null;
        }

        transform.position = position;

        if (!isTopCard)
        {
            gameObject.SetActive(false);
        }
    }
}
