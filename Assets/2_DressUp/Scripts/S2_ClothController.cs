﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class S2_ClothController : MonoBehaviour
{
    public static S2_ClothController clothBeingDragged;

    public Cloth clothType;

    private bool _isWaitingForMouseUp;
    private Vector3 _initialPosition;
    private bool _snapped;

    public enum Cloth
    {
        Dress,
        Shirt,
        Jacket,
        Pants,
        Shoes
    }

    private void Start()
    {
        _initialPosition = transform.localPosition;
    }

    private void OnMouseDown()
    {
        OnMouseDrag();
    }

    private void OnMouseDrag()
    {
        if (clothBeingDragged == null)
        {
            clothBeingDragged = this;

            if (!_isWaitingForMouseUp)
            {
                StartCoroutine(WaitForMouseUp());
            }
        }
        else if (clothBeingDragged == this)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);
        }
    }

    private IEnumerator WaitForMouseUp()
    {
        _isWaitingForMouseUp = true;

        yield return new WaitUntil(() => Input.GetMouseButtonUp(0));

        RaycastHit2D[] hit = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (hit.Length > 0 && hit.Any(h => h.collider.CompareTag("S2_Body")))
        {
            var body = hit.First(h => h.collider.CompareTag("S2_Body")).transform.GetComponent<S2_BodyController>();
            body.SnapToBody(this);
            _snapped = true;
        }
        else
        {
            StartCoroutine(GoBackToInitialPosition());

            if (_snapped)
            {
                FindObjectOfType<S2_BodyController>().Unsnap(clothType);
            }
        }

        clothBeingDragged = null;

        _isWaitingForMouseUp = false;
    }

    private IEnumerator GoBackToInitialPosition()
    {
        while (Vector3.Distance(transform.localPosition, _initialPosition) > 0.5f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, _initialPosition, Time.deltaTime * 5f);

            yield return null;
        }
    }
}
