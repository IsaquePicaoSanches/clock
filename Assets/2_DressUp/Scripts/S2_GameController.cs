﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class S2_GameController : MonoBehaviour
{
    void Awake() {
        Cursor.visible = true;
        Invoke("End", 3 * 60f);
    }

    void End()
    {
        SceneManager.LoadScene(0);
    }

    [Header("UI")]
    public Button finishedButton;

    public void ShowFinishedButton()
    {
        //finishedButton.gameObject.SetActive(true);
    }

    public void HideFinishedButton()
    {
        //finishedButton.gameObject.SetActive(false);
    }

    public void DoneBtnClick()
    {
        SceneManager.LoadScene(0);
    }
}
