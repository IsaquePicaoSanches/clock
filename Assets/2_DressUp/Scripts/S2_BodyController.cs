﻿using UnityEngine;

public class S2_BodyController : MonoBehaviour
{
    public S2_GameController gameController;

    [Header("Clothes transforms")]
    public Transform dressTransform;
    public Transform shirtTransform;
    public Transform pantsTransform;
    public Transform shoesTransform;

    private Transform _body;

    private bool _hasDress;
    private bool _hasShirt;
    private bool _hasPants;
    private bool _hasShoes;

    private bool _showDoneButton;

    private void Start()
    {
        _body = transform;
    }

    private void Update()
    {
        if (((_hasDress && _hasShoes) || (_hasPants && _hasShoes && _hasShirt)) && !_showDoneButton)
        {
            _showDoneButton = true;

            gameController.ShowFinishedButton();
        }
        else if((!_hasShoes || !_hasPants || !(_hasDress || _hasShirt)) && _showDoneButton)
        {
            _showDoneButton = false;

            gameController.HideFinishedButton();
        }
    }

    public void SnapToBody(S2_ClothController cloth)
    {
        Transform targetTransform = _body;

        switch (cloth.clothType)
        {
            case S2_ClothController.Cloth.Dress:
                targetTransform = dressTransform;
                _hasDress = true;
                break;
            case S2_ClothController.Cloth.Shirt:
            case S2_ClothController.Cloth.Jacket:
                targetTransform = shirtTransform;
                _hasShirt = true;
                break;
            case S2_ClothController.Cloth.Pants:
                targetTransform = pantsTransform;
                _hasPants = true;
                break;
            case S2_ClothController.Cloth.Shoes:
                targetTransform = shoesTransform;
                _hasShoes = true;
                break;
        }

        cloth.transform.position = targetTransform.position;
    }

    public void Unsnap(S2_ClothController.Cloth piece)
    {
        switch (piece)
        {
            case S2_ClothController.Cloth.Dress:
                _hasDress = false;
                break;
            case S2_ClothController.Cloth.Shirt:
                _hasShirt = false;
                break;
            case S2_ClothController.Cloth.Pants:
                _hasPants = false;
                break;
            case S2_ClothController.Cloth.Shoes:
                _hasShoes = false;
                break;
        }
    }
}
