﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s13_Ball : MonoBehaviour
{
    bool destroy = false;
    AudioSource ballKick;
    public AudioClip ballHit;

    // Start is called before the first frame update
    public void Awake()
    {
       ballKick = GetComponent<AudioSource>();
    }

    void Start()
    {
        ballKick.Play();
    }

    // Update is called once per frame
   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!destroy)
        {
            ballKick.clip = ballHit;
            ballKick.Play();
            Invoke("DestroyBall", 1);
            destroy = true;
        }
    }

    void DestroyBall()
    {
        Destroy(this.gameObject);
    }
}
