﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class s13_GM : MonoBehaviour
{

    public Transform babyDropper;
    public GameObject babyPrefab;
    public GameObject ballPrefab;
    public Transform ballDropper;

    float speed = 15;
    int turnprobability = 15;
    float timeToRespawn = 3;

    private void Start() {
        Cursor.visible = true;
        Invoke("GoBack", 45f);
        StartCoroutine(DropBabies());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject ball = Instantiate(ballPrefab, ballDropper.position, Quaternion.identity);
            Vector2 ballDirection = -(ballDropper.position - Camera.main.ScreenToWorldPoint( Input.mousePosition)).normalized ;
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(ballDirection.x,ballDirection.y) * 70;
            
        }
    }

    public void NewBaby()
    {
        GameObject baby = Instantiate(babyPrefab,babyDropper.position,Quaternion.identity);

        if(speed<25)
        speed += 0.5f;
        if(turnprobability<7)
        turnprobability--;
        baby.GetComponent<s13_Baby>().maxVelocity = speed;
        baby.GetComponent<s13_Baby>().maxRange= turnprobability;


    }
   
    IEnumerator DropBabies()
    {
        do
        {
            NewBaby();
            yield return new WaitForSeconds(timeToRespawn);
            if(timeToRespawn > 0.7f)
            {
                timeToRespawn -= 0.2f;
            }

        } while (true);
    }

    void GoBack()
    {
        SceneManager.LoadScene("mainScene");
    }
}
