﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s13_Baby : MonoBehaviour
{
    public int maxRange = 15;
    public float maxVelocity = 7;

    float speed = 0;
    s13_GM GM;
    SpriteRenderer sprite;
    Rigidbody2D babyRGBD;
    bool isHit = false;
    AudioSource babyCry;

    // Start is called before the first frame update
    void Start()
    {
        sprite = this.GetComponent<SpriteRenderer>();
        babyRGBD = this.GetComponent<Rigidbody2D>();
        babyCry = GetComponent<AudioSource>();
        Physics2D.IgnoreLayerCollision(12,12);
        GM = FindObjectOfType<s13_GM>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Random.Range(0, maxRange) == 1)
        {
            speed = Random.Range(-maxVelocity, maxVelocity);
            
        }
        if (!isHit)
            babyRGBD.velocity = new Vector2(speed, 0);

        if (speed < 0 && sprite.flipX)
            sprite.flipX = false;
        if (speed > 0 && !sprite.flipX)
            sprite.flipX = true;

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag== "ball")
        {
            babyRGBD.constraints = RigidbodyConstraints2D.None;
            isHit = true;
            babyCry.Play();
            babyCry.pitch = Random.Range(-1.5f, 1.5f);
        }
    }
    
    void Kill()
    {
        Destroy(this.gameObject);
    }
}
