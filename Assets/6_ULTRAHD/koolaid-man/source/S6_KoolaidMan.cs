﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S6_KoolaidMan : MonoBehaviour
{
    public float distanceToWalk = 5.0f;
    public float speed;
    public float delay;
    public AudioClip ohYeah;
    private Vector3 finalPos;
    private Vector3 posToSwitchSound;

    // Start is called before the first frame update
    void Start()
    {
        finalPos = transform.position;
        finalPos.z = distanceToWalk;
        posToSwitchSound = transform.position;
        posToSwitchSound.z = 0;
        StartCoroutine(Run());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Run()
    {
        yield return new WaitForSeconds(delay);
        while (Vector3.Distance(transform.position, finalPos) > 0.01)
        {
            transform.position = Vector3.MoveTowards(transform.position, finalPos, Time.deltaTime * speed);
            if (Vector3.Distance(transform.position, posToSwitchSound) <= 1.0f)
            {
                GetComponent<AudioSource>().clip = ohYeah;
                GetComponent<AudioSource>().Play();
            }
            yield return null;
        }
        yield return new WaitForSeconds(15.0f);
        SceneManager.LoadScene(0);
    }
}
