﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s18_boi : MonoBehaviour
{
    AudioSource Muuuuu;

   

    Coroutine Movement = null;

    private void Start()
    {
        Muuuuu = GetComponent<AudioSource>();
    }
    IEnumerator OnMovement()
    {
        Muuuuu.Play();
        yield return new WaitForSeconds(0.7f);

        GetComponent<Rigidbody2D>().velocity = new Vector2(40, 0);

        yield return new WaitForSeconds(2f);
        Muuuuu.Stop();
        Movement = null;
    }
   
    public void MovementStart(Vector2 StartPos)
    {
        if (Movement == null)
        {
            transform.position = StartPos;
            Movement= StartCoroutine(OnMovement());
        }

    }
   


}
