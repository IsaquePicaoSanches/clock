﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s18_enemy : MonoBehaviour
{
    s18_GameManager gameManager;
    bool isHit = false;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<s18_GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ball" && !isHit)
        {
            isHit = true;
            GetComponent<AudioSource>().Play();
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 10);
            gameManager.NextEnemyDelayed();
            
        }
    }


}
