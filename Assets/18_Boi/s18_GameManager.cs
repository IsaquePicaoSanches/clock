﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s18_GameManager : MonoBehaviour
{
    public s18_boi boi;
    Vector2 startPos;
    public Transform enemyDropper;
    public GameObject[] enemies;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        startPos = boi.transform.position;
        NextEnemy();
        Invoke("GoBack", 120);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            boi.MovementStart(startPos);
        }
    }


    public void NextEnemy()
    {
        if(index < enemies.Length)
        {
            GameObject enemy = Instantiate(enemies[index], enemyDropper.position, Quaternion.identity);
            index++;
        }
        else
        {
            GoBack();
        }
        
    }

    public void NextEnemyDelayed()
    {
        Invoke("NextEnemy", 1);
    }

    public void GoBack()
    {
        SceneManager.LoadScene("mainScene");
    }
}
