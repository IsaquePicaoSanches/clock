﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s3 : MonoBehaviour {

    public SpriteRenderer l1, l2, l3, l4, l5;
    public SpriteRenderer manyPics;
    int count = 0;

    private void Start() {
        Invoke("End", 1 * 40f);
    }

    void End() {
        SceneManager.LoadScene(0);
    }

    void Update() {

        if (Input.anyKeyDown)
            count++;

        if (count == 7) {
            count++;
            SceneManager.LoadScene(0);            
        }

        if (count > 7)
            return;

        manyPics.enabled = Input.anyKey;
        l1.enabled = (count == 0);
        l2.enabled = (count == 1);
        l3.enabled = (count == 2);
        l4.enabled = (count == 3);
        l5.enabled = (count >= 4);
    }
}
