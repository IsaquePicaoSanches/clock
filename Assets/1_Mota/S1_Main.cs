﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class S1_Main : MonoBehaviour {

    public Text secondsText;
    public Text blinkText;
    public Transform textA, textB;
    float t;
    void Start() {
        t = 0;
        int rand = Random.Range(0, 2);
        textA.gameObject.SetActive(rand == 0);
        textB.gameObject.SetActive(rand == 1);
    }

    void Update() {
        t += Time.deltaTime;
        blinkText.enabled = ((int)(t*3)) % 2 == 0;
        secondsText.text = "YOU HAVE " + (30-t).ToString("00") + " SECONDS";
        if (t > 30)
            SceneManager.LoadScene(0);
    }
}
