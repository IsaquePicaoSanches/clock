﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S10_NODDY : MonoBehaviour
{
    public float distanceToWalk = 5.0f;
    public float speed;
    public float delay;
    public GameObject koolAid;
    private Vector3 finalPos;
    private SpriteRenderer sr;

    // Start is called before the first frame update
    void Start()
    {
        finalPos = transform.position;
        finalPos.z = distanceToWalk;
        sr = GetComponent<SpriteRenderer>();
        StartCoroutine(Run());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Run()
    {
        yield return new WaitForSeconds(delay);
        while (Vector3.Distance(transform.position, finalPos) > 0.01)
        {
            transform.position = Vector3.MoveTowards(transform.position, finalPos, Time.deltaTime * speed);
            yield return null;
        }
        koolAid.SetActive(true);
        yield return new WaitForSeconds(4.0f);
        GetComponent<Rigidbody>().isKinematic = false;
        yield return new WaitForSeconds(10.0f);
        SceneManager.LoadScene(0);
    }

    IEnumerator Animate()
    {
        while(true)
        {
            sr.flipX = !sr.flipX;
            yield return new WaitForSeconds(0.3f);
        }
        
    }
}
