﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class S11_QuestionTyper : MonoBehaviour
{
    public string[] questions;
    string textString;
    Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        textString = questions[Random.Range(0, questions.Length)];
        text.text = "";
        StartCoroutine(TypeText());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator TypeText()
    {
        int charCount = 1;
        while(charCount <= textString.Length)
        {
            Debug.Log("Length " + textString.Length);
            text.text = textString.Substring(0, charCount);
            charCount++;
            yield return new WaitForSeconds(0.05f);
        }
        FindObjectOfType<S11_GameManager>().ShowAnswers();
    }
}
