﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S11_GameManager : MonoBehaviour
{
    public GameObject world;
    public GameObject helloWorldButton;
    public GameObject helloHumanText;
    public GameObject question;
    public GameObject answerButton1;
    public GameObject answerButton2;
    public GameObject okText;

    // Start is called before the first frame update
    void Start()
    {
        world.SetActive(false);
        helloHumanText.SetActive(false);
        question.SetActive(false);
        answerButton1.SetActive(false);
        answerButton2.SetActive(false);
        okText.SetActive(false);

        helloWorldButton.SetActive(true);

        Cursor.visible = true;

        Invoke("ExitScene", 180);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HelloWorldCallback()
    {
        StartCoroutine(ShowWorldAndAskQuestion());
    }

    IEnumerator ShowWorldAndAskQuestion()
    {
        helloWorldButton.SetActive(false);
        yield return null;
        world.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        helloHumanText.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        helloHumanText.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        question.SetActive(true);
    }

    public void ShowAnswers()
    {
        answerButton1.SetActive(true);
        answerButton2.SetActive(true);
    }

    public void AnswerCallback()
    {
        StartCoroutine(ShowOK());
    }

    IEnumerator ShowOK()
    {
        question.SetActive(false);
        answerButton1.SetActive(false);
        answerButton2.SetActive(false);
        yield return new WaitForSeconds(3.0f);
        okText.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene(0);
    }

    void ExitScene()
    {
        SceneManager.LoadScene(0);
    }
}
