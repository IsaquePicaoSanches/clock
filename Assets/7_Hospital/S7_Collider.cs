﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Activate Events on Trigger Enter
/// </summary>
[RequireComponent(typeof(Collider))]
public class S7_Collider : MonoBehaviour
{
    [SerializeField] private UnityEvent _OnTriggerEnter;
    [SerializeField,Tooltip("Other object Tag")] private string _otherTag;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_otherTag)){
            _OnTriggerEnter.Invoke();
        }
    }
}
