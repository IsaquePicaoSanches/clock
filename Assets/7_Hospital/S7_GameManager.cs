﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// Hospital Level GameManager
/// </summary>
public class S7_GameManager : MonoBehaviour
{
   [SerializeField] 
   private GameObject _player = null;
  
   [Header("Sounds:")]
   [SerializeField] private AudioClip _dancingBoyClip = null;
   [SerializeField] private AudioSource _audioSource = null;
   [SerializeField] private AudioClip _laughter = null;
   [SerializeField] private AudioSource _doorSource = null;
   [SerializeField] private AudioClip _doorClip = null;
   [SerializeField] private AudioSource[] _managerSounds;
   [SerializeField] private AudioClip _finalMusic;
   
   [SerializeField,Tooltip("Total time in seconds")] 
   private float _totalTime = 180;
   private float _timeLeft;

   [SerializeField] private GameObject _dancingBoy = null;
   private bool _dancingBoyActive = true;

   [SerializeField] private Animator _doorAnimator = null;

   [SerializeField] private GameObject _spawnPoint = null;
   [SerializeField] private GameObject[] _lights = null;
   [SerializeField] private GameObject _finalScene;
   [SerializeField] private GameObject _firstCollider;
   [SerializeField] private GameObject _secondCollider;
   [SerializeField] private GameObject _flashLight = null;
   [SerializeField] private FirstPersonController _fps = null;
   private bool _atTheEnd = false;

   private void Start()
   {
      Cursor.visible = false;
   }

   private void Update()
   {
      _timeLeft = _totalTime - Time.deltaTime;
      
      if (Input.anyKeyDown && _atTheEnd || _timeLeft <= 0){
         SceneManager.LoadScene(0);
      }
   }

   /// <summary>
   /// Hide or show dancing boy
   /// </summary>
   public void DancingBoy()
   {
      if (_dancingBoyActive){
         _dancingBoy.SetActive(false);
         _audioSource.PlayOneShot(_dancingBoyClip);
      } else {
         _dancingBoy.SetActive(true);
         _audioSource.PlayOneShot(_dancingBoyClip);
      }
      _firstCollider.SetActive(false);
   }

   /// <summary>
   /// Open ending door
   /// </summary>
   public void Door()
   {
      _secondCollider.SetActive(false);
      StartCoroutine(ENDINGSCENE());
   }

   /// <summary>
   /// Ending Scene sequence
   /// </summary>
   /// <returns></returns>
   private IEnumerator ENDINGSCENE()
   {
      _fps.enabled = false;
      _doorSource.PlayOneShot(_doorClip);
      _doorAnimator.SetTrigger("Open");
      yield return new WaitForSeconds(_doorClip.length/2);
      foreach (var light in _lights){
         light.SetActive(false);
      }

      foreach (var audio in _managerSounds){
         audio.enabled = false;
      }
      _flashLight.SetActive(false);
      _audioSource.PlayOneShot(_laughter);
      yield return new WaitForSeconds(_laughter.length/2);
      _player.transform.position = _spawnPoint.transform.position;
      _finalScene.SetActive(true);
      _flashLight.SetActive(true);
      _fps.enabled = true;
      _audioSource.PlayOneShot(_finalMusic);
      yield return new WaitForSeconds(5);
      SceneManager.LoadScene(0);
   }
}
