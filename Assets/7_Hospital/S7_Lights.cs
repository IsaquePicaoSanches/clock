﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Manage Lights 
/// </summary>
public class S7_Lights : MonoBehaviour
{
    [SerializeField,Tooltip("Light Source")] private Light _light;
    [SerializeField,Tooltip("Light Mesh Renderer")] private MeshRenderer _lightMeshRenderer;
    [SerializeField,Tooltip("Light on Material")] private Material _lightMaterialON;
    [SerializeField,Tooltip("Light Off Material")] private Material _lightMaterialOFF;
    [SerializeField,Tooltip("Min and Max interval time in seconds")] private int _min = 0, _max = 0;
    private float _interval;
    
    private void Start()
    {
        StartCoroutine(LightFlicker());
    }

    /// <summary>
    /// Flicker Lights
    /// </summary>
    private IEnumerator LightFlicker()
    {
        _interval = Random.Range(_min, _max);
        _light.enabled = false;
        _lightMeshRenderer.material = _lightMaterialOFF;
        yield return new WaitForSeconds(0.1f);
        _light.enabled = true;
        _lightMeshRenderer.material = _lightMaterialON;
        yield return new WaitForSeconds(0.1f);
        _light.enabled = false;
        _lightMeshRenderer.material = _lightMaterialOFF;
        yield return new WaitForSeconds(0.1f); 
        _light.enabled = true;
        _lightMeshRenderer.material = _lightMaterialON;
        yield return new WaitForSeconds(0.1f);
        _light.enabled = false;
        _lightMeshRenderer.material = _lightMaterialOFF;
        yield return new WaitForSeconds(0.1f);
        _light.enabled = true;
        _lightMeshRenderer.material = _lightMaterialON;
        yield return new WaitForSeconds(0.1f);
        _light.enabled = false;
        _lightMeshRenderer.material = _lightMaterialOFF;
        yield return new WaitForSeconds(0.1f); 
        _light.enabled = true;
        _lightMeshRenderer.material = _lightMaterialON;
        yield return new WaitForSeconds(0.1f);
        yield return new WaitForSeconds(_interval);
        StartCoroutine(LightFlicker());
    }
}
