﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s16_cart : MonoBehaviour
{
    Vector3 mousePosition;
    Rigidbody2D rigidbody2;
    public SpriteRenderer sprite;
    AudioSource audioSource;

    void Start()
    {
        rigidbody2 = GetComponent<Rigidbody2D>();
        //sprite = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
    }


    void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Mathf.Abs(transform.position.x - mousePosition.x) <0.5f)
        {
            rigidbody2.velocity = new Vector2(0, 0);
        }
        else if (transform.position.x > mousePosition.x)
        {
            rigidbody2.velocity = new Vector2(-20, 0);
        }
        else if(transform.position.x < mousePosition.x)
        {
            rigidbody2.velocity = new Vector2(20,0);
        }

        float t = 0;
        if (rigidbody2.velocity.x < 0) {
            t = Mathf.Clamp(force_Anim - (Time.deltaTime * 4f), -1f, 0);
        }
        else if (rigidbody2.velocity.x > 0) {
            t = Mathf.Clamp(force_Anim + (Time.deltaTime * 4f), 0f, 1f);
        }

        if (t == 1 && force_Anim != 1)
            sprite.flipX = false;
        if (t == -1 && force_Anim != -1)
            sprite.flipX = true;

        force_Anim = t;

        sprite.transform.position = Vector2.Lerp(sprite.transform.position, transform.position, Time.deltaTime * 15f);
        //Debug.Log(t);

        //sprite.flipX = rigidbody2.velocity.x < 0;

        // transform.position = new Vector3(mousePosition.x,transform.position.y,transform.position.z);
    }

    float force_Anim;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "ball")
        {
            collision.GetComponent<Rigidbody2D>().gravityScale = 15;
            audioSource.Play();
        }
    }
}
