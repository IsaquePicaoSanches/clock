﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s16_Raccon : MonoBehaviour
{
    public AudioClip eat;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(8, 13);
        Physics2D.IgnoreLayerCollision(8, 4);

        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
        Invoke("Kill", 3);
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "ball")
        {
            Destroy(collision.gameObject);
            audioSource.clip = eat;
            audioSource.Play();
        }
    }

    private void Kill()
    {
        Destroy(gameObject);
    }
}
