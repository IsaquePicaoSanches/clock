﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class s16_RacconDropper : MonoBehaviour
{

    public bool isRight = true;
    public GameObject RacconPrefab;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnRaccon());
    }

    IEnumerator SpawnRaccon()
    {
        do
        {
            yield return new WaitForSeconds(Random.Range(3, 12)*0.5f);

            speed = Random.Range(10, 30);
            GameObject raccoon = Instantiate(RacconPrefab, transform.position,Quaternion.identity);
            raccoon.GetComponent<Rigidbody2D>().velocity = new Vector2(isRight? speed : -speed, 0);
            raccoon.GetComponent<SpriteRenderer>().flipX = isRight;
        } while (true); 
    }
}
