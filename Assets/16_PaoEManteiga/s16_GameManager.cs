﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class s16_GameManager : MonoBehaviour
{
    public Transform Dropper;
    Vector3 dimension;
    public GameObject[] materials= null;
    float droppperY=2;
    float dropperTime = 1;
    

   public Text text;
    void Start()
    {
        dimension = Camera.main.ScreenToWorldPoint( new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight,0));
        Invoke("GoBack", 30);
        StartCoroutine(MakeItRain());
    }

    IEnumerator MakeItRain()
    {
        do
        {
            Instantiate(materials[Random.Range(0, 3)],  new Vector3( Random.Range(-(dimension.x/2), (dimension.x / 2)), Dropper.position.y, Dropper.position.z), Quaternion.identity);
            yield return new WaitForSeconds(0.8f);
            if (dropperTime > 0.1)
                dropperTime -= 0.01f;
        } while (true);
    }

    void GoBack()
    {
        SceneManager.LoadScene("mainScene");
    }

    float timer;
    public AudioSource audio;
    void Update() {
        timer += Time.deltaTime;
        int i = ((int)Mathf.Max(0, (15 - timer)));
        string s = "1.GET ALL THE FOOD\n2.AVOID THE RACOONS\n3.HARDCORE INDUSTRIAL POST-PUNK ACID METAL IN "+i.ToString("00")+" SECONDS";

        text.text = s;

        if (timer > 15f) {
            if (!audio.isPlaying)
                audio.Play();
            Camera.main.backgroundColor = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
        }

    }
}
