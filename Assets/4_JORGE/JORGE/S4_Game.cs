﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S4_Game : MonoBehaviour
{
    bool isPlaying = false;

    public GameObject idle;
    public GameObject dance;
    public GameObject background;

    public float timeToDance = 6.0f;

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPlaying && Input.GetKeyDown(KeyCode.Mouse0))
        {
            isPlaying = true;
            audioSource.Play();
            Invoke("Dance", timeToDance);
        }

        if (dance.activeSelf && Input.GetMouseButtonDown(0)) {
            SceneManager.LoadScene(0);
        }
    }

    void Dance()
    {
        idle.SetActive(false);
        dance.SetActive(true);
        background.SetActive(true);
        Invoke("GoToMain", 20.0f);
    }

    void GoToMain()
    {
        SceneManager.LoadScene(0);
    }
}
