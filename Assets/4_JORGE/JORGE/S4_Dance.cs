﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class S4_Dance : MonoBehaviour
{
    SpriteRenderer sr;
    public float timeToDab;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Dab();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Dab()
    {
        sr.flipX = !sr.flipX;
        Invoke("Dab", timeToDab);
    }
}
