﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S19_MarceloScript : MonoBehaviour
{
    public Vector3 finalEnterPos;
    public Vector3 finalExitPos;
    public Vector3 posToHide;
    public float enterSpeed;
    public float rotationSpeed;
    public GameObject webSummitLogo;
    AudioSource audioSource;
    bool canClick = false;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(Enter());
    }

    // Update is called once per frame
    void Update()
    {
        if (canClick && Input.GetMouseButtonDown(0))
        {
            canClick = false;
            StartCoroutine(IntoTheMind());
        }
    }

    IEnumerator Enter()
    {
        webSummitLogo.SetActive(false);
        while (Vector3.Distance(transform.position, finalEnterPos) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, finalEnterPos, Time.deltaTime * enterSpeed);
            yield return new WaitForEndOfFrame();
        }
        transform.position = finalEnterPos;
        webSummitLogo.SetActive(true);
        canClick = true;
    }

    IEnumerator IntoTheMind()
    {
        audioSource.Play();
        Vector3 newRot = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(newRot);

        Quaternion finalRotQuaternion = Quaternion.Euler(Vector3.zero);
        while (Quaternion.Angle(transform.rotation,finalRotQuaternion) > 1.0f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, finalRotQuaternion, rotationSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        transform.rotation = finalRotQuaternion;

        while (Vector3.Distance(transform.position, finalExitPos) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, finalExitPos, Time.deltaTime * enterSpeed);
            yield return new WaitForEndOfFrame();
        }
        transform.position = finalExitPos;

        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene(0);
    }
}
