﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class S14_audio : MonoBehaviour{

    [Range(0, 1)]
    public float range;
    float volumeCap;
    float noise;
    public List<AudioSource> audios;

    void Start() {
        audios = gameObject.GetComponents<AudioSource>().ToList();    
    }

    void Update(){
        if (range == 0)
            noise = 0;
        else
            noise = Mathf.Lerp(0.2f, 0.8f, range);

        volumeCap = Mathf.Lerp(0.9f, 0.6f, noise);
        volumeCap = Mathf.Max(0.33f, range) * volumeCap * 0.6f;
        float limit = 0.15f;
        float lerp = Mathf.Min(0, limit) / limit;
        audios[0].volume = Mathf.Lerp(0, volumeCap, lerp);
        limit = 0.3f;
        lerp = Mathf.Min(noise, limit) / limit;
        audios[1].volume = Mathf.Lerp(0, volumeCap, lerp);
        limit = 0.6f;
        lerp = Mathf.Min(noise, limit) / limit;
        audios[2].volume = Mathf.Lerp(0, volumeCap, lerp);
        limit = 0.7f;
        lerp = Mathf.Min(noise, limit) / limit;
        audios[3].volume = Mathf.Lerp(0, volumeCap, lerp);
        limit = 0.8f;
        lerp = Mathf.Min(noise, limit) / limit;
        audios[4].volume = Mathf.Lerp(0, volumeCap, lerp);
        limit = 0.9f;
        lerp = Mathf.Min(noise, limit) / limit;
        audios[5].volume = Mathf.Lerp(0, volumeCap, lerp);
        limit = 1f;
        lerp = Mathf.Min(noise, limit) / limit;
        audios[6].volume = Mathf.Lerp(0, volumeCap, noise);
    }
}
