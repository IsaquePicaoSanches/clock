﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S14_ : MonoBehaviour{

    public S14_audio audio_;
    [Range(0,1f)]
    public float noiseLevel = 0;
    public List<SpriteRenderer> sp;
    float completion;
    public SpriteRenderer completionRed;

    public Transform ramiFace;
    public enum Phase { One, Two, Three, Four, GamePlay, ThankYou};
    public Phase phase;
    public Animator arm;

    public GameObject screen1, screen2, screen3;

    void Start() {
        Invoke("End", 60f);
        Invoke("ScreenOneA", 4f);
        Invoke("ScreenOneB", 4+3f);
        Invoke("ScreenTwoA", 4+3+4f);
        Invoke("ScreenTwoB", 4+3+4+3f);
    }

    void ScreenOneA() {
        phase = Phase.Two;
        screen1.SetActive(true);
    }

    void ScreenOneB() {
        phase = Phase.Three;
        screen1.SetActive(false);
    }

    void ScreenTwoA() {
        phase = Phase.Four;
        screen2.SetActive(true);
    }

    void ScreenTwoB() {
        phase = Phase.GamePlay;
        screen2.SetActive(false);
    }

    void End() {
        if (phase == Phase.ThankYou)
            return;

        audio_.range = 0;
        Invoke("GoBack", 3);
        phase = Phase.ThankYou;
        screen3.SetActive(true);
    }

    void GoBack() {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    void Update() {
        ramiFace.transform.localPosition = Vector2.zero;

        if (phase == Phase.One)
            return;
        if (phase == Phase.Two)
            return;
        if (phase == Phase.Three) {
            arm.enabled = true;
            return;
        }
        if (phase == Phase.Four) {
            return;
        }
        if (phase == Phase.ThankYou) {
            return;
        }
     
       int index = (int)(sp.Count * noiseLevel);
        for (int i = 0; i < sp.Count; i++)
            sp[i].enabled = (i < index);

        float lerp = 0.5f * (Mathf.Sin(Time.time * Mathf.Lerp(10f, 50f, noiseLevel)) + 1);
        if (noiseLevel <= 0)
            lerp = 0;
        float h = (noiseLevel <= 0.05) ? 0 : Mathf.Lerp(0.1f, 0.15f, lerp);
        float t = Mathf.Cos(Time.time * 8f) * noiseLevel * 0.05f;
        float n = Mathf.Cos(Time.time * 100f) * noiseLevel * 0.01f;
        ramiFace.localPosition = new Vector2(0, h + t + n);
        ramiFace.rotation = Quaternion.Slerp(ramiFace.rotation, Quaternion.Euler(new Vector3(0, 0, lerp*noiseLevel*(Mathf.Cos(Time.time * 40) * 10f))), Time.deltaTime * 20f);

        if (Input.anyKey) {
            noiseLevel = Mathf.Min(noiseLevel + Time.deltaTime * 0.3f, 1f);
            audio_.range = Mathf.Min(noiseLevel + Time.deltaTime * 0.33f, 1f);
        } else {
            noiseLevel = Mathf.Lerp(noiseLevel, 0, Time.deltaTime * 10f);
            audio_.range = 0;
        }

        if (noiseLevel > 0.33f)
            completion += Mathf.Min(1f, Time.deltaTime * 0.1f * Mathf.Lerp(1.1f, 0.01f, completion) * Mathf.Lerp(2f, 0.01f, completion) * Mathf.Lerp(1f, 0.05f, completion));
        else
            completion = Mathf.Lerp(completion, 0, Time.deltaTime * 20);

        completionRed.transform.localPosition = new Vector3(0, Mathf.Lerp(-13f, 0f, completion), 10);
        completionRed.color = new Color(0.6981132f, 0.02963687f, 0.0775753f, Mathf.Lerp(0.4f, 1f, completion));

        //Debug.Log(completion);

        if (completion >= 0.62f)
            End();
    }
}
