﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorAndScreen : MonoBehaviour {

    public GameObject cursorPrefab;
    void Start() {
        float ratio = 16f / 9f;
        Screen.SetResolution((int)(ratio * Screen.height), Screen.height, true);
        cursorPrefab = Instantiate(cursorPrefab);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update() {
        cursorPrefab.transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
