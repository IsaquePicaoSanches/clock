﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Mover : MonoBehaviour
{
    Waypoint previousWaypoint;
    Waypoint currentWaypoint;
    Waypoint nextWaypoint;
    float dist = 0;
    float t = 0;
    public float speed = 10;

    public void Awake() {
        List<Waypoint> wp = new List<Waypoint>();
        wp = FindObjectsOfType<Waypoint>().ToList();
        currentWaypoint = wp[Random.Range(0, wp.Count)];
        wp.Clear();
        nextWaypoint = currentWaypoint.waypoints[Random.Range(0, currentWaypoint.waypoints.Count)];
        dist = Vector2.Distance(currentWaypoint.transform.position, nextWaypoint.transform.position);
        rand = Random.Range(0, 2f) * Mathf.PI;
        transform.position = currentWaypoint.transform.position;
    }

    float rand = 0;
    public void Update() {
        float sin = ((1f + Mathf.Sin((Time.time + rand) * 0.33f)) * 0.5f);
        sin = 1 - (sin * 0.66f);
        t += (Time.deltaTime * speed * sin) / dist;
        if (t > 1f)
        {
            t = 0;
            Waypoint temp = currentWaypoint;
            currentWaypoint = nextWaypoint;
            nextWaypoint = currentWaypoint.waypoints[Random.Range(0, currentWaypoint.waypoints.Count)];
            if (previousWaypoint != null)
            {
                for (int i = 0; i < 100; i++) {
                    if (nextWaypoint == previousWaypoint)
                    nextWaypoint = currentWaypoint.waypoints[Random.Range(0, currentWaypoint.waypoints.Count)];
                }
            }
            previousWaypoint = temp;
            dist = Vector2.Distance(currentWaypoint.transform.position, nextWaypoint.transform.position);
        }
        transform.position = Vector2.Lerp(currentWaypoint.transform.position, nextWaypoint.transform.position, t);
    }

    public Vector3 GetDirection() {
        Vector2 v = nextWaypoint.transform.position - currentWaypoint.transform.position;
        v = v.normalized;
        float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        return new Vector3(0, 0, angle+90f);
    }
}
