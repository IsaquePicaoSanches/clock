﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {
    public List<Waypoint> waypoints;

    void OnDrawGizmos(){
        Gizmos.color = Color.cyan;
        foreach (Waypoint p in waypoints)
            Gizmos.DrawLine(transform.position, p.transform.position);
    }

}


