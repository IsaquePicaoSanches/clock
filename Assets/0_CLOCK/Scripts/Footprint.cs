﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footprint : MonoBehaviour{
    float time = 25;
    float totalTime = 0;

    public SpriteRenderer sp;
    void Start(){
        totalTime = time;
        int x = Random.Range(0, 2);
        int y = Random.Range(0, 2);
        sp.flipX = x == 0;
        sp.flipY = y == 0;
    }

    void Update() {
        sp.color = Color.Lerp(new Color(1, 1, 1, 0.8f), new Color(1, 1, 1, -0.1f), 1-(time / totalTime));
        time -= Time.deltaTime;
        if (time <= 0)
            Destroy(this.gameObject);
    }
}
