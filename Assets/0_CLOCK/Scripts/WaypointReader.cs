﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointReader : MonoBehaviour{
    
    public List<Waypoint> waypoints;
    private void Awake() {

        for (int i = 0; i < waypoints.Count-1; i++){
            if (!waypoints[i].waypoints.Contains(waypoints[(i + 1) % waypoints.Count]))
                waypoints[i].waypoints.Add(waypoints[(i + 1) % waypoints.Count]);

            if (!waypoints[(i + 1) % waypoints.Count].waypoints.Contains(waypoints[i]))
                waypoints[(i + 1) % waypoints.Count].waypoints.Add(waypoints[i]);
        }
    }

    /*
    void OnDrawGizmos(){
        Gizmos.color = Color.cyan;
        for (int i = 0; i < waypoints.Count; i++){
            Vector2 posA = waypoints[i].transform.position;
            Vector2 posB = waypoints[(i + 1) % waypoints.Count].transform.position;
            if (i + 1 == waypoints.Count)
                Gizmos.color = Color.green;
            Gizmos.DrawLine(posA, posB);
        }
    }
    */
}
