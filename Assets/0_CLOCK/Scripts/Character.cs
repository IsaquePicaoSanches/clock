﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;

public class Character : MonoBehaviour {

    public GameObject footprintPrefab;
    public Mover targetMover;
    public Transform body;
    public Transform balloonStuff;
    public Balloon balloon;

    Vector2 realPosition;
    float distance = 0;
    bool leftStep;
    public bool stopped;
    
    void Start() {
        realPosition = targetMover.transform.position;
        transform.position = realPosition;
    }    

    void Update() {
        targetMover.speed = stopped ? 0f : 0.33f;
        balloon.visibility = Mathf.Lerp(balloon.visibility, stopped ? 1 : 0, Time.deltaTime * 4f);
        balloon.big = stopped ? balloon.mouseOver : false;

        realPosition = Vector2.Lerp(realPosition, targetMover.transform.position, Time.deltaTime * 1f);
        distance += Vector2.Distance(realPosition, transform.position);
        if (distance > 0.1f) {//HARDCODED
            Vector3 direction = targetMover.GetDirection();
            GameObject gj = Instantiate(footprintPrefab, realPosition, Quaternion.Euler(direction));
            gj.transform.localScale = new Vector3(leftStep ? -1 : 1, 1f, 1f);
            leftStep = !leftStep;
            distance = 0;
        }
        transform.position = realPosition;
        if (!stopped)
            balloonStuff.transform.localScale = new Vector3((realPosition.x > 3.5f) ? -1 : 1, (realPosition.y > 1.1f) ? -1 : 1, 1f);
        //body.rotation = Quaternion.Slerp(body.rotation, Quaternion.Euler(targetMover.GetDirection()), Time.deltaTime * 1f);
    }     
}
