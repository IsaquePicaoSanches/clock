﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DiscoPlayer : MonoBehaviour{
    public static DiscoPlayer instance;
    static int count;
    public AudioSource audiosource;
    public List<AudioClip> clips;

    public static void Stop() {
        instance.audiosource.Stop();
    }

    public static void Play() {
        instance.audiosource.clip = instance.clips[count];
        count = (count + 1) % instance.clips.Count;
        instance.audiosource.Play();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Return))
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
