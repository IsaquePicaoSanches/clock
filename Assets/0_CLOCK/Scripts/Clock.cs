﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Clock : MonoBehaviour{

    public GameObject listenToPeople;
    public Text message;
    public GameObject messageCanvas;
    public GameObject creditsCanvas;
    //int sceneToLoadIndex;

    public GameObject characterPrefab;
    public GameObject moverPrefab;
    public EventList events;
    List<Character> characters;

    void Awake() {
        characters = new List<Character>();
        for (int i = 0; i < 6; i++) {
            Character c = Instantiate(characterPrefab).GetComponent<Character>();
            c.gameObject.name = "char_" + i;
            characters.Add(c);

            Mover m = Instantiate(moverPrefab).GetComponent<Mover>();
            m.gameObject.name = "mover_" + i;

            c.targetMover = m;
        }

        StartCoroutine(CreateLoop());
        
    }

    public static List<int> sceneIndexes;
    public static int sceneCount;
    public GameObject discoPrefab;
    void Start() {
        if (DiscoPlayer.instance == null) {
            DiscoPlayer.instance = Instantiate(discoPrefab).GetComponent<DiscoPlayer>();
            DiscoPlayer.instance.clips.Shuffle();
            DontDestroyOnLoad(DiscoPlayer.instance.gameObject);
        }
        DiscoPlayer.Play();

        if (sceneIndexes == null) {
            sceneIndexes = new List<int>();
            for (int i = 1; i < SceneManager.sceneCountInBuildSettings; i++)
                sceneIndexes.Add(i);
            sceneIndexes.Shuffle();
        }
    }

    public Text clockTextA, clockTextB, clockTextC, clockTextD;
    public static float progress = 0f;
    public static int globalIndex;
    float lastProgress = 0;
    bool freeze;
    float timeHack;

    IEnumerator LoadScene(int i) {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(i);
        asyncLoad.allowSceneActivation = false;
        yield return new WaitForSeconds(2);
        DiscoPlayer.Stop();
        asyncLoad.allowSceneActivation = true;
    }

    float totalSeconds = 60 * 6f;
    void Update() {
        listenToPeople.SetActive(((int)(Time.time*2)) % 2 == 0);
        timeHack += Time.deltaTime;

        if (freeze) 
            return;
        
        progress = (progress + ((Time.deltaTime * 1.2f) / (totalSeconds))) % 1f;

        if (progress < lastProgress)
            globalIndex++;
        lastProgress = progress;

        int seconds = (int)Mathf.Lerp(0, totalSeconds, 1-progress);
        var timespan = System.TimeSpan.FromSeconds(seconds);
        List<char> f = timespan.ToString(@"mm\:ss").ToCharArray().ToList();
        string a = f[0] + "";
        clockTextA.text = a;
        string b = f[1] + "";
        clockTextB.text = b;
        string c = f[3] + "";
        clockTextC.text = c;
        string d = f[4] + "";
        clockTextD.text = d;

        if (Input.GetMouseButtonUp(0)) {
            if (creditsCanvas.activeSelf)
                creditsCanvas.SetActive(false);
            else if (messageCanvas.activeSelf)  {
                int scene = sceneIndexes[sceneCount % sceneIndexes.Count];// Mathf.Max(0, sceneToLoadIndex);
                if (scene != 0) {
                    freeze = true;
                    sceneCount++;
                    StartCoroutine(LoadScene(scene));
                }
                else {
                    messageCanvas.SetActive(false);
                    foreach (Character char_ in characters)
                        char_.stopped = false;
                    StopAllCoroutines();
                    StartCoroutine(CreateLoop());
                }
            }
        }
        if (Input.GetMouseButtonDown(0) && timeHack > 0.5f) {
            if (characters.Any(x => x.balloon.big))
                messageCanvas.SetActive(true);
            else
                creditsCanvas.SetActive(true);
        }
    }


    IEnumerator CreateLoop() {
        //REMAP TIMES AND CHARS
        
        float step = events.moments.Count;
        step = 1f / step;
        for (int i = 0; i < events.moments.Count; i++) {
            events.moments[i].startingPoint = i * step;
            events.moments[i].characterIndex = i % characters.Count;
        }        

        List<EventList.Moment> momentList_ = events.moments.OrderBy(x => x.startingPoint).ToList();
        List<EventList.Moment> momentList = momentList_;
        momentList_ = momentList_.Where(x => x.startingPoint < progress).ToList();
        momentList = momentList.Where(x => x.startingPoint >= progress).ToList();
        foreach (EventList.Moment m in momentList_)
            momentList.Add(m);


        yield return new WaitForSeconds(totalSeconds*(momentList[0].startingPoint - progress));
        for (int i = 0; i < momentList.Count; i++) {
            int index = momentList[i].characterIndex;
            //int sceneIndex = //Random.Range(1, SceneManager.sceneCountInBuildSettings);// ((i+rand) % (SceneManager.sceneCountInBuildSettings - 1)) + 1;
            StartCoroutine(
                SpeechBubble(characters[index], momentList[i].sentence)
                );
            yield return new WaitForSeconds(totalSeconds * (momentList[(i+1)% momentList.Count].startingPoint - momentList[i].startingPoint));
        }

        StartCoroutine(CreateLoop());
    }

    IEnumerator SpeechBubble(Character c, string s) {
        foreach (Character char_ in characters)
            char_.stopped = false;

        if (messageCanvas.activeSelf)
            yield break;

        c.stopped = true;
        message.text = s.ToUpper();
        //sceneToLoadIndex = index;
        yield return new WaitForSeconds(12f);
        //sceneToLoadIndex = -1;
        c.stopped = false;
    }
}
