﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Balloon : MonoBehaviour {

    public float visibility;
    public bool big;
    public Animator fakeAnchor;
    float t_red2black = 0;
    public SpriteRenderer red, black;
    public Text text;
    public bool mouseOver = false;

    void Update() {
        fakeAnchor.SetBool("big", big);

        t_red2black = Mathf.Clamp(t_red2black + ((big ? 1 : -1) * Time.deltaTime * 3f), 0, 1f);
        float t = Mathf.Min(t_red2black, 0.8f)/0.8f;
        red.color = Color.Lerp(new Color(1, 1, 1, visibility), new Color(1, 1, 1, 0f), t);
        t = Mathf.Min(t_red2black, 0.4f) / 0.4f;
        black.color = Color.Lerp(new Color(1, 1, 1, 0f), new Color(1, 1, 1, visibility), t);

        text.color = new Color(text.color.r, text.color.g, text.color.b, visibility);

        transform.localPosition = Vector3.Lerp(transform.localPosition, fakeAnchor.transform.localPosition, Time.deltaTime * 3f);
        transform.localScale = Vector3.Lerp(transform.localScale, fakeAnchor.transform.localScale, Time.deltaTime * 3f);
    }

    void OnMouseEnter() {
        mouseOver = true;
    }

    void OnMouseExit() {
        mouseOver = false;
    }
}
