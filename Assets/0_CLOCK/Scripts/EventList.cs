﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EventList", menuName = "ScriptableObjects/EventList", order = 1)]
public class EventList : ScriptableObject {

    public List<Moment> moments;

    [System.Serializable]
    public class Moment {
        //public enum Character { A, B, C, D, E, F};

        [Header("Strings")]
        public string sentence;
        public int sceneIndex;
        [Header("Timming")]
        public int characterIndex;
        //[HideInInspector]
        [Range(0, 1)]
        public float startingPoint;
    }
}

