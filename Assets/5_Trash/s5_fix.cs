﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s5_fix : MonoBehaviour{

    private void Start() {
        Cursor.visible = true;
        Invoke("End", 3 * 60f);
    }

    void End() {
        SceneManager.LoadScene(0);
    }

}
