﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S5_DraggableTrashPiece : MonoBehaviour
{
    public S5_TrashPiece trashPiece;
    Vector3 originalPos;
    bool isOnRightDumpster = false;

    bool isDragging;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = trashPiece.sprite;
        originalPos = transform.position;

        Vector2 S = gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size;
        gameObject.GetComponent<BoxCollider2D>().size = S;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<S5_Dumpster>() && collision.GetComponent<S5_Dumpster>().trashType == trashPiece.trashType)
        {
            isOnRightDumpster = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<S5_Dumpster>() && collision.GetComponent<S5_Dumpster>().trashType == trashPiece.trashType)
        {
            isOnRightDumpster = false;
        }
    }

    private void OnMouseDown()
    {
        isDragging = true;
        Debug.Log("Mouse Down");
    }

    private void OnMouseDrag()
    {

        Debug.Log("Mouse Drag");
        if (isDragging)
        {
            //transform.position = Input.mousePosition;
            Vector3 screenPoint = Input.mousePosition;
            screenPoint.z = 10.0f; //distance of the plane from the camera
            transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
        }
    }

    private void OnMouseUp()
    {
        Debug.Log("Mouse Up");
        isDragging = false;
        if (isOnRightDumpster)
        {
            FindObjectOfType<S5_TrashManager>().CreateNextPiece();
            Destroy(gameObject);
        } else
        {
            transform.position = originalPos;
        }
    }
}
