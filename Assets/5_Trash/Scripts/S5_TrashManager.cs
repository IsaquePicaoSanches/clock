﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class S5_TrashManager : MonoBehaviour
{
    public S5_TrashPiece[] trashPieces;
    public GameObject DraggableTrashPiecePrefab;
    int trashCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        CreateNextPiece();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateNextPiece()
    {
        if (trashCount >= trashPieces.Length)
        {
            SceneManager.LoadScene(0);
            return;
        }
        GameObject piece = Instantiate(DraggableTrashPiecePrefab, transform);
        piece.GetComponent<S5_DraggableTrashPiece>().trashPiece = trashPieces[trashCount];
        trashCount++;
    }
}
