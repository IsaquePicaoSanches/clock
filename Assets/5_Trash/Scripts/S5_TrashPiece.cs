﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TrashPiece", order = 1)]
public class S5_TrashPiece : ScriptableObject
{
    public enum TrashType
    {
        Glass,
        Paper,
        Plastic,
        Toxic,
        None
    }

    public Sprite sprite;
    public TrashType trashType;
}
